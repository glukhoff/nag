package own.my.nagapp;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ScanFragment extends Fragment implements OnItemClickListener {

    public static final String TAG = "ScanFragment";
    static final int SECURITY_NONE = 0;
    static final int SECURITY_WEP = 1;
    static final int SECURITY_PSK = 2;
    static final int SECURITY_EAP = 3;
    //Short for Organizational Unique Identifier, the first 24 bits of a MAC address for a network-connected device
    private static final String NAG_OUI = "F8:F0:82";//check here https://mac-oui.com/?q=F8%3AF0%3A82
    private static final String MY_OUI = "60:31:97";
    BroadcastReceiver mReceiver;
    String mPassword;
    private WifiManager mWifiManager;
    private ListView mListView;
    private List<ScanResult> mResults;
    private List<String> mStrings = new ArrayList<>();
    private ArrayAdapter mAdapter;
    private OnScanFragmentInteractionListener mListener;

    public static ScanFragment newInstance(@Nullable Bundle args) {
        ScanFragment fragment = new ScanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * this is stub (хранилище паролей не в рамках данного ТЗ)
     */
    private static void savePassword(Context context, String key, String pass) {
//        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, pass).apply();
    }

    private static boolean isHex(String value) {
        boolean ret;
        try {
            int t = Integer.parseInt(value, 16);
            ret = true;
            Timber.d("hex %d", t);

        } catch (NumberFormatException e) {
            ret = false;
        }
        return (ret);
    }

    @Nullable
    private static ScanResult scanResultOfSsid(@NonNull final List<ScanResult> results, @NonNull final String ssid) {
        for (ScanResult result : results) {
            if (result.SSID.equals(ssid)) {
                return result;
            }
        }
        return null;
    }

    static int getSecurity(ScanResult result) {
        if (result.capabilities.contains("WEP")) {
            return SECURITY_WEP;
        } else if (result.capabilities.contains("PSK")) {
            return SECURITY_PSK;
        } else if (result.capabilities.contains("EAP")) {
            return SECURITY_EAP;
        }
        return SECURITY_NONE;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        mListView = (ListView) inflater.inflate(R.layout.fragment_scan_list, container, false);


        final FragmentActivity activity = getActivity();
        if (activity != null) {
            MyPermission.checkAll(activity);
        }

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Timber.d("onReceive");
                if (MyPermission.hasPermission(context, ACCESS_FINE_LOCATION)) {
                    mResults = mWifiManager.getScanResults();
                    unregisterReceiver(context, this);

                    for (ScanResult result : mResults) {
                        Timber.d("SSID %s, BSSID %s", result.SSID, result.BSSID);//bssid == mac-address
                        String oui = TextUtils.substring(result.BSSID, 0, 8);
                        Timber.d("oui %s", oui);
                        Timber.d("capabilities %s", result.capabilities);
//                if (oui.equals(MY_OUI)) {//todo replace with NAG_OUI
                        mStrings.add(result.SSID);
                        mAdapter.notifyDataSetChanged();
//                }
                    }

                }
            }
        };

        final Context context = getContext();
        if (context == null) {
            Timber.w("null context");
            return mListView;
        }
        mWifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (mWifiManager != null && !mWifiManager.isWifiEnabled()) {
            MyNotifier.notifyUserToast(context, R.string.string_turning_on_wifi);
            mWifiManager.setWifiEnabled(true);
        }

        mAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, mStrings);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        scanWiFi(context);

        return mListView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnScanFragmentInteractionListener) {
            mListener = (OnScanFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnScanFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        final Context context = getContext();
        if (mReceiver != null && context != null) {
            unregisterReceiver(context, mReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.resetTitle();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Timber.d("onConfigurationChanged");
        super.onConfigurationChanged(newConfig);


    }

    void scanWiFi(@NonNull final Context context) {
        Timber.d("scanWiFi");
        if (mReceiver != null) {
            unregisterReceiver(context, mReceiver);
            context.registerReceiver(mReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            mStrings.clear();
            mWifiManager.startScan();
            MyNotifier.notifyUserToast(context, R.string.string_choose_access_point);
        }

    }

    private void unregisterReceiver(@NonNull Context context, @NonNull BroadcastReceiver receiver) {
        Timber.d("unregisterReceiver");
        try {
            context.unregisterReceiver(receiver);
        } catch (Exception e) {
            Timber.w("already registered");
        }
    }

    /**
     * выбор сети
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //highlight view (make bold italic)
        TextView text1 = view.findViewById(android.R.id.text1);
        final ArrayAdapter adapter = (ArrayAdapter) parent.getAdapter();

        //После выбора сети производится подключение (wifi)
        String ssid = (String) adapter.getItem(position);
        Timber.d("ssid %s", Strings.nullToEmpty(ssid));
        final ScanResult result = scanResultOfSsid(mResults, ssid);
        if (result == null) {
            Timber.e("unreachable code");
            return;
        }
        final int security = getSecurity(result);

        if (security == SECURITY_NONE) {//no need pass
            //подсветить
            highlightItem(text1, adapter);

            //коннект
            view.post(new RunnableConnect(view.getContext(), security, ssid));

        } else {//need pass

            //check wifi connection
            if (security == SECURITY_PSK && isCurrentlyConnected(ssid)) {//return
                //подсветить
                highlightItem(text1, adapter);

                //сообщение, что уже connected
                MyNotifier.notifyUserToast(view.getContext(),R.string.string_already_connected);

                //connect via SSH
                connectSSH(ssid);
                return;
            }

            //получить из хранилища паролей
            String pass = retrievePassword(getContext(), ssid);

            if (Strings.isNullOrEmpty(pass)) {//request and run
                getInputText(R.string.string_enter_password_wifi, new RunnableConnect(view.getContext(), security, ssid, text1, adapter));

            } else {//run on the spot
                view.post(new RunnableConnect(view.getContext(), security, ssid, pass));
            }
        }
    }

    private void highlightItem(@Nullable final TextView text1, @Nullable final ArrayAdapter adapter) {
        if (text1 != null && adapter != null) {
            text1.setTypeface(null, Typeface.BOLD_ITALIC);
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 1)connect SSH , 2)open screen2 (ConfigFragment) if successful
     */
    synchronized void connectSSH(@NonNull final String ssid) {
        Timber.d("connectSSH(%s)", ssid);
        final String macOui = getMacOuiBySsid(ssid);

        Bundle params = getParamsByMacOui(macOui);
        final Context context = getContext();
        if (context == null) {
            return;
        }

        if (params == null) {
            MyNotifier.notifyUserToast(context, R.string.string_no_ssh_params_for_this_oui);
            return;
        }

        MyNotifier.notifyUserToast(context, R.string.string_connecting_SSH);

        final Executor executor = ((MyApp) context.getApplicationContext()).getAppExecutors().networkIO();
        executor.execute(() -> {
            final FragmentActivity activity = getActivity();
            SSHHelper.progressBar(true, activity);

            Bundle results = SSHHelper.getConfigBulk(
                    params.getString(SSHHelper.SSH_USER, ""),
                    params.getString(SSHHelper.SSH_PASSWORD, ""),
                    params.getString(SSHHelper.SSH_HOST, ""),
                    params.getInt(SSHHelper.SSH_PORT, 0),
                    (ExecutorService) executor);

            SSHHelper.progressBar(false, activity);

            if (results != null) {//success, open next screen
                Timber.d("goto screen 2");
                if (mListener != null && activity != null) {
                    activity.runOnUiThread(() -> mListener.showFragment(results, ConfigFragment.TAG, false));
                }

            } else {//failed SSH
                String msg = String.format(Locale.US, "%s (SSH)", context.getString(R.string.string_could_not_connect));
                Timber.w(msg);
                MyNotifier.notifyUserToast(context, msg);
            }

        });
    }

    /**
     * получить параметры для коннекции SSH для данного Mac Oui из хранилища
     * this is stub,  хранилище Mac Oui не надо реализовывать в рамках данного ТЗ
     */
    @Nullable
    Bundle getParamsByMacOui(@NonNull final String macOui) {
        Timber.d("getParamsByMacOui(%s)", macOui);
        Bundle params = new Bundle();
//        if (macOui.equals(NAG_OUI)) {//todo раскомметировать при наличии такого маршрутизатора
        params.putString(SSHHelper.SSH_USER, "Admin_Test");
        params.putString(SSHHelper.SSH_PASSWORD, "qzectbsfh");//todo не править, т.к. в рамках данного ТЗ не реализовано хранилище
        params.putString(SSHHelper.SSH_HOST, "kazancev.ddns.net");
        params.putInt(SSHHelper.SSH_PORT, 9322);
        return params;
//        }
//        return null;//not found
    }

    private String getMacOuiBySsid(@NonNull final String ssid) {
        for (ScanResult result : mResults) {
            if (result.SSID.equals(ssid)) {
                return TextUtils.substring(result.BSSID, 0, 8);
            }
        }
        throw new IllegalStateException(String.format(Locale.US, "ssid %s not found", ssid));
    }

    /**
     * this is stub (хранилище паролей не в рамках данного ТЗ)
     */
    private String retrievePassword(Context context, @NonNull final String key) {
//        String pass = PreferenceManager.getDefaultSharedPreferences(context).getString(key, "");
//        if (!Strings.isNullOrEmpty(pass)) {
//            return pass;
//        }
        return null;
    }

    private void getInputText(@StringRes final int title, @NonNull final Runnable runnable) {
        Timber.d("getInputText");
        final Context context = getContext();

        mPassword = "";
        final EditText input = new EditText(context);//view.findViewById(R.id.password);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_CLASS_TEXT);
        AlertDialog.Builder builder = new Builder(context);
        builder.setTitle(title)
                .setView(input)
                .setNegativeButton(R.string.string_cancel, (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.string_ok, (dialog, which) -> {
                    Timber.d("onClick");
                    mPassword = input.getText().toString();
                    Timber.d("mPassword %s", mPassword);
                    if (runnable instanceof RunnableConnect) {
                        RunnableConnect runnableConnect = (RunnableConnect) runnable;
                        runnableConnect.setPass(mPassword);
                    }

                    runnable.run();
                    dialog.dismiss();
                }).setCancelable(false)
                .show();
    }

    /**
     * for WPA only
     */
    private boolean isCurrentlyConnected(@NonNull String ssid) {
        Timber.d("isCurrentlyConnected(%s)", ssid);
        WifiInfo info = mWifiManager.getConnectionInfo();
        final String infoSSID = info.getSSID();
        Timber.d("active ssid %s", infoSSID);
        boolean connected = false;

        if (!TextUtils.isEmpty(infoSSID) &&
                (infoSSID.equals(ssid) || infoSSID.equals(SSHHelper.convertToQuotedString(ssid)))) {
            DetailedState state = WifiInfo.getDetailedStateOf(info.getSupplicantState());
            Timber.d("DetailedState %s", state.toString());
            connected = state == DetailedState.CONNECTED || state == DetailedState.OBTAINING_IPADDR;
        }

        Timber.d("connected %b", connected);
        return connected;
    }

    /**
     * попытка подключить Wifi
     *
     * @return id сети (success) or  -1 (failed)
     */
    private int connectWifi(int security, @NonNull final String ssid, @Nullable String pass) {
        Timber.d("connectWifi(security %d, ssid %s, pass %s)", security, ssid, Strings.nullToEmpty(pass));
        WifiConfiguration wfc = new WifiConfiguration();

        wfc.SSID = SSHHelper.convertToQuotedString(ssid);//"\"".concat(ssid).concat("\"");
        wfc.status = WifiConfiguration.Status.DISABLED;
        wfc.priority = 40;

        switch (security) {//на данный момент при редактировании доступны OPEN и WPA2-PSK (из ТЗ)
            case SECURITY_NONE:
                configureOpenNetwork(wfc);
                break;

            case SECURITY_WEP:
                configureWepNetwork(wfc, pass);
                break;

            case SECURITY_PSK:
//                if (ssid.equals("Keenetic-6595")) {
//                    pass = "LsURUAwF";
//                }
                configurePskNetwork(wfc, pass);
                break;

            //what about EAP - не в рамках данного ТЗ
        }

        int network = mWifiManager.addNetwork(wfc);
        Timber.d("network %d", network);

        List<WifiConfiguration> configurations = mWifiManager.getConfiguredNetworks();
        for (WifiConfiguration configuration : configurations) {
//            Timber.d("ssid %s", configuration.SSID);
            if (configuration.SSID != null && configuration.SSID.equals(SSHHelper.convertToQuotedString(ssid))) {
                Timber.d("found in configurations: %s", ssid);
                mWifiManager.disconnect();
                mWifiManager.enableNetwork(configuration.networkId, true);
                boolean reconnected = mWifiManager.reconnect();
                Timber.d("reconnected %b", reconnected);

                network = reconnected ? configuration.networkId : -1;
                Timber.d("configuration.networkId %d", configuration.networkId);
                break;
            }
        }

        return network;
    }

    private void configurePskNetwork(@NonNull final WifiConfiguration wfc, @Nullable String password) {
        Timber.d("configurePskNetwork, pass %s", Strings.nullToEmpty(password));
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

        wfc.preSharedKey = SSHHelper.convertToQuotedString(password);
    }

    private void configureWepNetwork(@NonNull final WifiConfiguration wfc, @Nullable String password) {
        Timber.d("configureWepNetwork");
        wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

        if (isHex(password)) wfc.wepKeys[0] = password;
        else wfc.wepKeys[0] = SSHHelper.convertToQuotedString(password);
        wfc.wepTxKeyIndex = 0;
    }

    private void configureOpenNetwork(@NonNull final WifiConfiguration wfc) {
        Timber.d("configureOpenNetwork");
        wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wfc.allowedAuthAlgorithms.clear();
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
    }

    public interface OnScanFragmentInteractionListener {
        void showFragment(@Nullable final Bundle args, @NonNull final String tag, boolean addStack);
    }

    /**
     * сначала реализует подключение wifi, если ОК, то инициирует попытку подключение SSH
     */
    private class RunnableConnect implements Runnable {
        Context context;
        int security;
        String ssid;
        String pass;

        TextView textView;
        ArrayAdapter adapter;

        RunnableConnect(@NonNull Context context, int security, @NonNull final String ssid) {
            Timber.d("RunnableConnect(security %d, ssid %s, pass %s)", security, ssid, Strings.nullToEmpty(pass));
            this.context = context;
            this.security = security;
            this.ssid = ssid;
        }

        RunnableConnect(@NonNull Context context, int security, @NonNull final String ssid, @NonNull final String pass) {
            Timber.d("RunnableConnect(security %d, ssid %s, pass %s)", security, ssid, Strings.nullToEmpty(pass));
            this.context = context;
            this.security = security;
            this.ssid = ssid;
            this.pass = pass;
        }

        RunnableConnect(@NonNull Context context, int security, @NonNull final String ssid, @Nullable final TextView textView, @Nullable final ArrayAdapter adapter) {
            Timber.d("RunnableConnect(security %d, ssid %s, pass %s)", security, ssid, Strings.nullToEmpty(pass));
            this.context = context;
            this.security = security;
            this.ssid = ssid;
            this.textView = textView;
            this.adapter = adapter;
        }

        void setPass(String pass) {
            this.pass = pass;
        }

        @Override
        public void run() {
            Timber.d("run");
            //connect wifi
            final int connected = connectWifi(this.security, this.ssid, this.pass);
            if (connected != -1) {//success
                //highlight item
                highlightItem(textView, adapter);

                //msg
                String msg = String.format(Locale.US, "%s (wifi)", context.getString(R.string.string_connected));
                MyNotifier.notifyUserToast(context, msg);

                //save pass wifi
                savePassword(context, ssid, pass);

                //connect SSH
                connectSSH(this.ssid);

            } else {//failed
                //msg
                String msg = String.format(Locale.US, "%s (wifi)", context.getString(R.string.string_could_not_connect));
                MyNotifier.notifyUserToast(context, msg);

            }
        }

    }
}
