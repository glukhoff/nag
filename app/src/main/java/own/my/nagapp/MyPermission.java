package own.my.nagapp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import java.util.Locale;

import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CHANGE_WIFI_STATE;
import static android.Manifest.permission.INTERNET;

public class MyPermission {

    private static final int PERMISSION_REQUEST_ALL = 127;


    private static boolean isNewPermissionModel() {
        return VERSION.SDK_INT >= VERSION_CODES.M;
    }

    public static boolean hasPermission(@NonNull Context context, @NonNull String permission) {
        return !isNewPermissionModel() || (ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * возвращает false, если не все permissions, true - когда все!
     */
    private static boolean hasPermissions(@NonNull Context context, @NonNull String... permissions) {
        if (isNewPermissionModel()) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    static void checkAll(@NonNull final Activity activity) {
        Timber.d("checkAll");

        //check dangerous permissions, make request if need (Android will ask only for the ones it needs)

        String[] PERMISSIONS = {
                ACCESS_WIFI_STATE,
                CHANGE_WIFI_STATE,
                ACCESS_FINE_LOCATION,
                INTERNET
        };

        if (!hasPermissions(activity,PERMISSIONS)) {//если не все permissions, тогда запросить
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    PERMISSION_REQUEST_ALL);
        }
    }


    private static void reRequestPermissions(@NonNull final Activity activity, @NonNull final String permission, @NonNull final String msg) {

        if (isNewPermissionModel() && ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {// If the user turned down the permission request in the past and chose the Don't ask again option in the permission request system dialog, this method returns false.

            //Show an explanation to the user with action
            MyNotifier.notifyUser(activity, msg, 0, 2, R.string.string_ok, v -> checkAll(activity));

        }
    }
    /**
     * check response for dangerous permissions (calendar, location)
     */
    static void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults, @NonNull final MainActivity activity) {
        Timber.d("onRequestPermissionsResult");
        if (requestCode == PERMISSION_REQUEST_ALL) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {

                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {//granted
                        Timber.d("%d %s granted", i, permissions[i]);
                        if (permissions[i].equals(ACCESS_FINE_LOCATION)) {
                            //rescan
                            activity.reScan();
                        }

                    } else { //not granted
                        final String msg = String.format(Locale.US, "%s : %s", activity.getString(R.string.string_permission_required), permissions[i]);
                        reRequestPermissions(activity, permissions[i], msg);

                    }


                }
            }
        }
    }

}
