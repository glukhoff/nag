package own.my.nagapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import timber.log.Timber;

public class SSHHelper {

    public static final String SSH_USER = "user";
    public static final String SSH_PASSWORD = "password";
    public static final String SSH_HOST = "host";
    public static final String SSH_PORT = "port";
    static final String DEVNAME = "DEVNAME";
    static final String VERSIONPKG = "VERSIONPKG";
    static final String VERSIONSTR = "VERSIONSTR";
    static final String SSID_1 = "SSID1";
    static final String SSID_1_INIC = "SSID1INIC";
    static final String WPAPSK_1 = "WPAPSK1";
    static final String WPAPSK_1_INIC = "WPAPSK1INIC";
    static final String AUTH_MODE = "AuthMode";
    static final String LOGIN = "Login";
    static final String PASSWORD = "Password";
    public static final String RESULT = "result";
    public static final String DELIMITER = "\n";
    static final String S_VERSION = "sVersion";
    static final String S_NV_RAM = "sNVRam";


    /**
     * 1) connect SSH, 2)read version from file, 3)read nvars
     * @return bundle with results of 2)3)
     */
    static Bundle getConfigBulk(@NonNull final String user, @NonNull final String pass, @NonNull final String host, final int port, @NonNull final ExecutorService service) {
        Timber.d("getConfig");
        try {
            //get version from remote file
            Future<String> future = service.submit(() -> readExec(user, pass, host, port, "cat /etc/version"));

            String sVersion = future.get();
            Timber.d("sVersion %s", Strings.nullToEmpty(sVersion));

            //get NVRam vars
            Future<String> future2 = service.submit(() -> readExec(user, pass, host, port, "nvram_buf_get 2860 SSID1 SSID1INIC WPAPSK1 WPAPSK1INIC AuthMode Login Password"));

            String sNVRam = future2.get();
            Timber.d("sNVRam %s", Strings.nullToEmpty(sNVRam));

            if (!TextUtils.isEmpty(sVersion) && !TextUtils.isEmpty(sNVRam)) {
                Bundle results = new Bundle();
                results.putString(S_VERSION, sVersion);
                results.putString(S_NV_RAM, sNVRam);
                results.putString(SSH_USER, user);
                results.putString(SSH_PASSWORD, pass);
                results.putString(SSH_HOST, host);
                results.putInt(SSH_PORT, port);
                return results;
            }

        } catch (Exception e) {
            MyExceptions.log2Crashlytics(e);
        }
        return null;
    }

    static Map<String, String> string2Map(@NonNull final String string, Pattern compile, String separator) {
        Timber.d("string2Map(%s)", string);
        Splitter.MapSplitter mapSplitter = Splitter
                .on(compile)
                .withKeyValueSeparator(separator);

        return mapSplitter.split(string);
    }

    @WorkerThread
    static boolean writeExec(@NonNull final String user, @NonNull final String pass, @NonNull final String host, final int port, @NonNull final String cmd) {
        Timber.d("writeExec");
        try {
            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelExec exec = (ChannelExec) session.openChannel("exec");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            exec.setOutputStream(stream);
            exec.setCommand(cmd);
            exec.connect();
            Thread.sleep(1000);
            exec.disconnect();
            session.disconnect();
            Timber.d("stream.toString() %s", Strings.nullToEmpty(stream.toString())) ;

            return true;

        } catch (Exception e) {
            MyExceptions.log2Crashlytics(e);
            return false;
        }
    }

    @WorkerThread
    @Nullable
    static String readExec(@NonNull final String user, @NonNull final String pass, @NonNull final String host, final int port, @NonNull final String cmd) {
        Timber.d("readExec");
        try {
            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelExec exec = (ChannelExec) session.openChannel("exec");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            exec.setOutputStream(stream);
            exec.setCommand(cmd);
            exec.connect();
            Thread.sleep(1000);
            exec.disconnect();
            session.disconnect();
            return stream.toString();

        } catch (Exception e) {
            MyExceptions.log2Crashlytics(e);
        }

        return null;
    }

    static void doExec(@NonNull final String user, @NonNull final String pass, @NonNull final String host, final int port, @NonNull final String cmd) {
        Timber.d("doExec");
        try {
            JSch jSch = new JSch();
            Session session = jSch.getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelExec exec = (ChannelExec) session.openChannel("exec");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            exec.setOutputStream(stream);
            exec.setCommand(cmd);
            exec.connect();
            Thread.sleep(1000);
            exec.disconnect();
            session.disconnect();
            Timber.d("stream.toString() %s", Strings.nullToEmpty(stream.toString())) ;

        } catch (Exception e) {
            MyExceptions.log2Crashlytics(e);
        }
    }

    static String convertToQuotedString(String string) {
        return "\"" + string + "\"";
    }

    static String convertToUnQuotedString(String string) {
        return string.replace("\"", "");
    }


    public static synchronized void progressBar(boolean turn, @Nullable final Activity activity) {
        Timber.d("progressBar(%b)", turn);
        if (activity != null && activity instanceof MainActivity) {
            ((MainActivity)activity).setWaitScreen(turn);
        } else {
            Timber.w("null activity");
        }
    }

}
