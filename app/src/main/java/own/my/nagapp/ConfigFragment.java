package own.my.nagapp;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import timber.log.Timber;

import static own.my.nagapp.R.id.auth_mode;
import static own.my.nagapp.R.id.auth_mode_label;
import static own.my.nagapp.R.id.button_edit;
import static own.my.nagapp.R.id.dev_name;
import static own.my.nagapp.R.id.dev_name_label;
import static own.my.nagapp.R.id.login;
import static own.my.nagapp.R.id.login_label;
import static own.my.nagapp.R.id.password;
import static own.my.nagapp.R.id.password_label;
import static own.my.nagapp.R.id.ssid_1;
import static own.my.nagapp.R.id.ssid_1_inic;
import static own.my.nagapp.R.id.ssid_1_inic_label;
import static own.my.nagapp.R.id.ssid_1_label;
import static own.my.nagapp.R.id.version_pkg;
import static own.my.nagapp.R.id.version_pkg_label;
import static own.my.nagapp.R.id.version_str;
import static own.my.nagapp.R.id.wpa_psk_1;
import static own.my.nagapp.R.id.wpa_psk_1_inic;
import static own.my.nagapp.R.id.wpa_psk_1_inic_label;
import static own.my.nagapp.R.id.wpa_psk_1_label;
import static own.my.nagapp.SSHHelper.AUTH_MODE;
import static own.my.nagapp.SSHHelper.DEVNAME;
import static own.my.nagapp.SSHHelper.LOGIN;
import static own.my.nagapp.SSHHelper.PASSWORD;
import static own.my.nagapp.SSHHelper.SSH_HOST;
import static own.my.nagapp.SSHHelper.SSH_PASSWORD;
import static own.my.nagapp.SSHHelper.SSH_PORT;
import static own.my.nagapp.SSHHelper.SSH_USER;
import static own.my.nagapp.SSHHelper.SSID_1;
import static own.my.nagapp.SSHHelper.SSID_1_INIC;
import static own.my.nagapp.SSHHelper.S_NV_RAM;
import static own.my.nagapp.SSHHelper.S_VERSION;
import static own.my.nagapp.SSHHelper.VERSIONPKG;
import static own.my.nagapp.SSHHelper.VERSIONSTR;
import static own.my.nagapp.SSHHelper.WPAPSK_1;
import static own.my.nagapp.SSHHelper.WPAPSK_1_INIC;
import static own.my.nagapp.SSHHelper.convertToQuotedString;
import static own.my.nagapp.SSHHelper.convertToUnQuotedString;
import static own.my.nagapp.SSHHelper.doExec;
import static own.my.nagapp.SSHHelper.string2Map;
import static own.my.nagapp.SSHHelper.writeExec;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnConfigFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConfigFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfigFragment extends Fragment {
    static public final String TAG = "ConfigFragment";
    public static final String BUTTON_TAG_EDITING = "editing";
    public static final String BUTTON_TAG_HOME = "home";//"home" in xml !!!
    public static final int TIMER_PROGRESS_MILLIS = 20000;

    //credentials
    private String mUser;
    private String mPass;
    private String mHost;
    private int mPort;

    //block 1 (версия прошивки)
    String mDevName;
    String mVersionPkg;
    String mVersionStr;

    //block 2 (настройки б/п сети)
    String mSsid1;
    String mSsid1Inic;
    String mWpaPsk1;
    String mWpaPsk1Inic;
    String mAuthMode;

    //block 3 (информация о пользователе)
    String mLogin;
    String mPassword;

    private OnConfigFragmentInteractionListener mListener;

    @BindView(dev_name_label) TextView lblDevName;
    @BindView(version_pkg_label) TextView lblVersionPkg;
    @BindView(ssid_1_label) TextView lblSsid1;
    @Nullable @BindView(ssid_1_inic_label) TextView lblSsid1Inic;
    @BindView(auth_mode_label) TextView lblAuthMode;
    @BindView(wpa_psk_1_label) TextView lblWpaPsk1;
    @Nullable @BindView(wpa_psk_1_inic_label) TextView lblWpaPsk1Inic;
    @BindView(login_label) TextView lblLogin;
    @BindView(password_label) TextView lblPassword;


    @BindView(dev_name) TextView txDevName;
    @BindView(version_pkg) TextView txVersionPkg;
    @BindView(version_str) TextView txVersionStr;

    @BindView(ssid_1) EditText etSsid1;
    @Nullable @BindView(ssid_1_inic) EditText etSsid1Inic;
    @BindView(auth_mode) EditText etAuthMode;
    @BindView(wpa_psk_1) EditText etWpaPsk1;
    @Nullable @BindView(wpa_psk_1_inic) EditText etWpaPsk1Inic;

    @BindView(login) EditText etLogin;
    @BindView(password) EditText etPassword;

    @BindView(button_edit) Button btButton;

    @BindViews({ssid_1, ssid_1_inic, auth_mode, wpa_psk_1, wpa_psk_1_inic, login, password})
    List<EditText> editables;

    @BindViews({ssid_1_inic_label, wpa_psk_1_inic_label, ssid_1_inic, wpa_psk_1_inic})
    List<View> visibles;

    @OnTextChanged(ssid_1)
    void onTextChangedSsid1(CharSequence text) {
        Timber.d("onTextChangedSsid1");
        configChanged = true;
        changed_ssid1 = true;
    }

    @OnTextChanged(ssid_1_inic)
    void onTextChangedSsid1Inic(CharSequence text) {
        Timber.d("onTextChangedSsid1Inic");
        configChanged = true;
        changed_ssid1_inic = true;
    }

    @OnTextChanged(auth_mode)
    void onTextChangedAuthMode(CharSequence text) {
        Timber.d("onTextChangedAuthMode");
        configChanged = true;
        changed_auth_mode = true;
    }

    @OnTextChanged(wpa_psk_1)
    void onTextChangedWpaPsk1(CharSequence text) {
        Timber.d("onTextChangedWpaPsk1");
        configChanged = true;
        changed_wpa_psk1 = true;
    }

    @OnTextChanged(wpa_psk_1_inic)
    void onTextChangedWpaPsk1Inic(CharSequence text) {
        Timber.d("onTextChangedWpaPsk1Inic");
        configChanged = true;
        changed_wpa_psk1_inic = true;
    }

    @OnTextChanged(login)
    void onTextChangedLogin(CharSequence text) {
        Timber.d("onTextChangedLogin");
        configChanged = true;
        changed_login=true;
    }

    @OnTextChanged(password)
    void onTextChangedPassword(CharSequence text) {
        Timber.d("onTextChangedPassword");
        configChanged = true;
        changed_password = true;
    }

    private Unbinder mUnbinder;

    private boolean configChanged = false;
    private boolean changed_ssid1 = false;
    private boolean changed_ssid1_inic = false;
    private boolean changed_auth_mode = false;
    private boolean changed_wpa_psk1 = false;
    private boolean changed_wpa_psk1_inic = false;
    private boolean changed_login = false;
    private boolean changed_password = false;


    private static final ButterKnife.Setter<EditText, Boolean> EDITABLE = (view, value, index) -> {
        if (value) {
            view.setEnabled(true);
            view.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        } else {
            view.setEnabled(false);
            view.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        }
    };

    private static final ButterKnife.Setter<View, Boolean> VISIBLE = (view, value, index)->{
            view.setVisibility(value? View.VISIBLE: View.GONE);
    };

    public ConfigFragment() {
        // Required empty public constructor
    }

    public static ConfigFragment newInstance(@NonNull final Bundle args) {
        ConfigFragment fragment = new ConfigFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        final Bundle args = getArguments();
        if (args == null) {
            MyExceptions.log2Crashlytics(new IllegalStateException("null args"));
            return;
        }

        String sVersion = args.getString(S_VERSION);
        Timber.d("sVersion %s", sVersion);

        //block 1 (not editable)
        if (!Strings.isNullOrEmpty(sVersion)) {
            Map<String, String> map1 = string2Map(sVersion.trim(), Pattern.compile("\r?\n"), " = ");
            mDevName = convertToUnQuotedString(map1.get(DEVNAME));
            mVersionPkg = convertToUnQuotedString(map1.get(VERSIONPKG));
            mVersionStr = convertToUnQuotedString(map1.get(VERSIONSTR));
            Timber.d("mDevName %s, DEVNAME %s", Strings.nullToEmpty(mDevName), DEVNAME);
            Timber.d("mVersionPkg %s, VERSIONPKG %s", Strings.nullToEmpty(mVersionPkg), VERSIONPKG);
            Timber.d("mVersionStr %s, VERSIONSTR %s", Strings.nullToEmpty(mVersionStr), VERSIONSTR);
        }

        //block 2 (editable)
        String sNVRam = args.getString(S_NV_RAM);
        if (!Strings.isNullOrEmpty(sNVRam)) {
            Map<String, String> map2 = string2Map(sNVRam.trim(), Pattern.compile("\r?\n"), "=");
            mSsid1 = convertToUnQuotedString(map2.get(SSID_1));
            mSsid1Inic = convertToUnQuotedString(map2.get(SSID_1_INIC));
            mWpaPsk1 = convertToUnQuotedString(map2.get(WPAPSK_1));
            mWpaPsk1Inic = convertToUnQuotedString(map2.get(WPAPSK_1_INIC));
            mAuthMode = convertToUnQuotedString(map2.get(AUTH_MODE));
            mLogin = convertToUnQuotedString(map2.get(LOGIN));
            mPassword = convertToUnQuotedString(map2.get(PASSWORD));
            Timber.d("mLogin %s, LOGIN %s", mLogin, LOGIN);
            Timber.d("mPassword %s, PASSWORD %s", mPassword, PASSWORD);
        }

        //credentials
        mUser = args.getString(SSH_USER);
        mPass = args.getString(SSH_PASSWORD);
        mHost = args.getString(SSH_HOST);
        mPort = args.getInt(SSH_PORT);
        Timber.d("mUser %s, mPass %s, mHost %s, mPort %d", mUser, mPass, mHost, mPort);

        //menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_config, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        displayData();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Timber.i("onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Timber.d("onOptionsItemSelected");
        //toggle locale - on activity level

        //refresh views
        final View view = getView();
        if (view != null) {
            lblDevName.setText(R.string.string_device_model);
            lblVersionPkg.setText(R.string.string_firmware_version);
            lblSsid1.setText(R.string.string_wireless_network);
            if (lblSsid1Inic != null) {
                lblSsid1Inic.setText(R.string.string_wireless_network_5);
            }
            lblAuthMode.setText(R.string.string_encryption_method);
            lblWpaPsk1.setText(R.string.string_wireless_network_password);
            if (lblWpaPsk1Inic != null) {
                lblWpaPsk1Inic.setText(R.string.string_wireless_network_password_5);
            }
            lblLogin.setText(R.string.string_user);
            lblPassword.setText(R.string.string_password);

            final String tag = (String) btButton.getTag();
            if (Strings.isNullOrEmpty(tag) || tag.equals(BUTTON_TAG_HOME)) {
                btButton.setText(R.string.string_edit);
            } else {
                btButton.setText(R.string.string_save);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener != null) {
            mListener.resetTitle();
        }

    }

    private void displayData() {
        //block 1
        txDevName.setText(mDevName);
        txVersionPkg.setText(mVersionPkg);
        txVersionStr.setText(mVersionStr);

        //block 2
        etSsid1.setText(mSsid1);
        etAuthMode.setText(mAuthMode);
        etWpaPsk1.setText(mWpaPsk1);

        //Имя беспроводной сети для 5ГГц (переменная SSID1INIC) - отображается только если модель маршрутизатора отличается от: SNR-CPE-W2N-MT / SNR-CPE-W4N-MT / SNR-CPE-AP1-MT
        List<String> list = Arrays.asList("SNR-CPE-W2N-MT", "SNR-CPE-W4N-MT", "SNR-CPE-AP1-MT");
        if (contains(mDevName, list)) {//5GHz gone
            ButterKnife.apply(visibles, VISIBLE, false);

        } else {//отличается, отображать 5GHz
            ButterKnife.apply(visibles, VISIBLE, true);
            if (!TextUtils.isEmpty(mSsid1Inic) && etSsid1Inic != null) {
                etSsid1Inic.setText(mSsid1Inic);
            }
            if (!TextUtils.isEmpty(mWpaPsk1Inic) && etWpaPsk1Inic != null) {
                etWpaPsk1Inic.setText(mWpaPsk1Inic);
            }

        }

        //block 3
        etLogin.setText(mLogin);
        etPassword.setText(mPassword);

        //prohibit editing
        ButterKnife.apply(editables, EDITABLE, false);
    }

    @OnClick(button_edit)
    void onClickButton() {
        Timber.d("onClickButton");
        final String tag = (String) btButton.getTag();
        if (Strings.isNullOrEmpty(tag) || tag.equals(BUTTON_TAG_HOME)) {//to edit mode

            //retag
            btButton.setTag(BUTTON_TAG_EDITING);

            //rename button
            btButton.setText(R.string.string_save);

            //reset flag
            configChanged = false;
            changed_ssid1 = false;
            changed_ssid1_inic = false;
            changed_auth_mode = false;
            changed_wpa_psk1 = false;
            changed_wpa_psk1_inic = false;
            changed_login = false;
            changed_password = false;

            //make fields editable
            ButterKnife.apply(editables, EDITABLE, true);

        } else {//save, off edit mode

            //retag button
            btButton.setTag(BUTTON_TAG_HOME);

            //rename button
            btButton.setText(R.string.string_edit);

            //off editable
            ButterKnife.apply(editables, EDITABLE, false);

            if (configChanged) {//save

                //prepare map of changes
                Map<String, String> changes = prepareMapOfChanges();

                if (changes.size() != 0) {
                    //get executor
                    final Executor executor =  ((MyApp) btButton.getContext().getApplicationContext()).getAppExecutors().networkIO();
                    executor.execute(() -> {
                        final FragmentActivity activity = getActivity();

                        if (activity != null) {
                            //lock button
                            activity.runOnUiThread(() -> btButton.setEnabled(false));
                        }

                        //turn on progress
                        SSHHelper.progressBar(true, activity);

                        //toast
                        MyNotifier.notifyUserToast(btButton.getContext(), R.string.string_saving_and_reloading);

                        //save SSH
                        saveSSH(changes, (ExecutorService) executor);


                        //reboot SSH
                        rebootSSH((ExecutorService)executor);


                        try {
                            Thread.sleep(TIMER_PROGRESS_MILLIS);
                        } catch (InterruptedException e) {
                            Timber.e(e);
                        }

                        //turn off progress
                        SSHHelper.progressBar(false, activity);

                        if (activity != null) {
                            //unlock button
                            activity.runOnUiThread(() -> btButton.setEnabled(true));

                            if (mListener != null) {
                                //goto screen 1
                                activity.runOnUiThread(() -> mListener.showFragment(null, ScanFragment.TAG, false));
                            }
                        }
                    });
                }

            } else {
                Timber.d("nothing to save");
                MyNotifier.notifyUserToast(btButton.getContext(), R.string.string_nothing_to_save);
            }
        }
    }

    @NonNull
    Map<String, String> prepareMapOfChanges() {
        Timber.d("prepareMapOfChanges");
        Map<String, String> changes = new LinkedHashMap<>();
        if (changed_ssid1) {
            changes.put(SSID_1, etSsid1.getText().toString());
        }
        if (changed_ssid1_inic && etSsid1Inic != null) {
            changes.put(SSID_1_INIC, etSsid1Inic.getText().toString());
        }
        if (changed_wpa_psk1) {
            changes.put(WPAPSK_1, etWpaPsk1.getText().toString());
        }
        if (changed_wpa_psk1_inic && etWpaPsk1Inic != null) {
            changes.put(WPAPSK_1_INIC, etWpaPsk1Inic.getText().toString());
        }
        if (changed_auth_mode) {
            changes.put(AUTH_MODE, etAuthMode.getText().toString());
        }
        if (changed_login) {
            changes.put(LOGIN, etLogin.getText().toString());
        }
        if (changed_password) {
            changes.put(PASSWORD, etPassword.getText().toString());
        }
        Timber.d("size of map %d", changes.size());
        return changes;
    }
    /**
     * save config SSH, all
     * использую здесь exec, хотя можно попробовать и shell (for multiple commands)
     */
    private void saveSSH(@NonNull final Map<String, String> changes, @NonNull final ExecutorService service) {
        Timber.d("saveSSH");
        for (Map.Entry<String, String> entry : changes.entrySet()) {
            try {
                final String cmd = prepareSaveCmd(entry.getKey(), entry.getValue());
                Timber.d("cmd: %s", cmd);

                final Future<Boolean> future = service.submit(() -> writeExec(mUser, mPass, mHost, mPort, cmd));
                Timber.d("future.get() %b", future.get());

            } catch (Exception e) {
                MyExceptions.log2Crashlytics(e);
            }
        }
    }

    private void rebootSSH(@NonNull final ExecutorService service) {
        Timber.d("rebootSSH");
        service.execute(()-> doExec(mUser, mPass, mHost, mPort, "reboot"));
    }

    /**
     * prepare cmd for saving pair (like nvram_set 2860 SSID1 "Wive-NG-MT-TST")
     */
    private String prepareSaveCmd(@NonNull final String key, @NonNull final String value) {
        return String.format(Locale.US, "nvram_set 2860 %s %s", key, convertToQuotedString(value));
    }

    private boolean contains(@NonNull final String what, Iterable<String> list) {//https://stackoverflow.com/a/21457777/6905587
        Timber.d("contains(%s)", what);
        final boolean contains = FluentIterable.from(list).contains(what);
        Timber.d("return %b", contains);
        return contains;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConfigFragmentInteractionListener) {
            mListener = (OnConfigFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnConfigFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        super.onDestroyView();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnConfigFragmentInteractionListener {

        void toggleLocale();

        void resetTitle();

        public void showFragment(@Nullable final Bundle args, @NonNull final String tag, boolean addStack);
    }
}
