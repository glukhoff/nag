package own.my.nagapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

abstract class DeferredFragmentTransaction {

    private Fragment mReplacingFragment;

    @Nullable
    private String mTag;
    private boolean mAddStack;

    @Nullable
    private String mStackName;



    public abstract void commit();

    Fragment getReplacingFragment() {
        return mReplacingFragment;
    }

    void setReplacingFragment(@NonNull Fragment replacingFragment) {
        mReplacingFragment = replacingFragment;
    }

    @Nullable
    String getTag() {
        return mTag;
    }

    public void setTag(@Nullable String tag) {
        mTag = tag;
    }

    boolean isAddStack() {
        return mAddStack;
    }

    void setAddStack(boolean addStack) {
        mAddStack = addStack;
    }

    @Nullable
    String getStackName() {
        return mStackName;
    }

    void setStackName(@Nullable String stackName) {
        mStackName = stackName;
    }
}
