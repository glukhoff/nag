package own.my.nagapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.Button;

public class ExitConfirmationDialog extends DialogFragment {
    static final String TAG = "ExitConfirmationDialog";

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = getContext();
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setMessage(R.string.string_want_to_exit)
                    .setCancelable(false)
                    .setPositiveButton(R.string.string_exit, (dialog, which) -> {

                                dialog.dismiss();

                                final MainActivity activity = (MainActivity) getActivity();
                                //exit
                                if (activity != null) {
                                    activity.doExit();
                                }
                            }
                    )
                    .setNegativeButton(R.string.string_cancel, (dialog, which) -> dialog.dismiss());


            final AlertDialog alertDialog = builder.create();
            //https://stackoverflow.com/a/15910202/6905587
            alertDialog.setOnShowListener(dialog -> {
                Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                int width  = getResources().getDimensionPixelSize(R.dimen.alertdialog_button_min_width);
                int height = getResources().getDimensionPixelSize(R.dimen.alertdialog_button_min_height);
                btnPositive.setMinimumWidth(width);
                btnPositive.setMinimumHeight(height);

                Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setMinimumWidth(width);
                btnNegative.setMinimumHeight(height);
            });
            return alertDialog;

        }
        return getDialog();
    }

    @Override
    public void onDestroyView() {
        if (getView() instanceof ViewGroup) {
            ((ViewGroup)getView()).removeAllViews();
        }
        super.onDestroyView();
    }

}
