package own.my.nagapp;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.transition.Fade;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.common.base.Strings;

import java.util.ArrayDeque;
import java.util.Queue;

import own.my.nagapp.ConfigFragment.OnConfigFragmentInteractionListener;
import own.my.nagapp.ScanFragment.OnScanFragmentInteractionListener;
import timber.log.Timber;

import static own.my.nagapp.LocaleManager.LANGUAGE_ENGLISH;
import static own.my.nagapp.LocaleManager.LANGUAGE_RUSSIAN;
import static own.my.nagapp.LocaleManager.getLanguage;
import static own.my.nagapp.LocaleManager.persistLanguage;
import static own.my.nagapp.LocaleManager.setLocale;
import static own.my.nagapp.LocaleManager.updateResource2;

public class MainActivity extends AppCompatActivity
        implements OnConfigFragmentInteractionListener, OnScanFragmentInteractionListener {
    public static final String M_BACK_PRESSED = "mBackPressed";
    private final Queue<DeferredFragmentTransaction> deferredFragmentTransactions = new ArrayDeque<>();//queue of pending fragment transactions, https://medium.com/@bendaniel10/a-possible-way-to-safely-perform-fragment-transactions-after-activity-onsaveinstancestate-651d4bcb410b
    private boolean isRunning;//check pause and resume activity
    private Menu mMenu;
    private int mWaitScreenCnt = 0;//progress bar cnt
    private long mBackPressed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");

        //get current lang
        final String currentLanguage = getLanguage(this);

        //update configuration
        updateResource2(this, currentLanguage);

        //reset title
        resetTitle();


        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {//todo
            Timber.i("restoring...");
            mBackPressed = savedInstanceState.getLong(M_BACK_PRESSED, 0);
        }

        showFragment(null, ScanFragment.TAG, true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Timber.i("onSaveInstanceState");
        outState.putLong(M_BACK_PRESSED, mBackPressed);
    }


    @Override
    public void onBackPressed() {
        Timber.d("onBackPressed");
        //обработка doubleclicks
        if (mBackPressed + 1000 > System.currentTimeMillis()) {
            Timber.d("doubleclick back pressed");
            doExitAfterConfirmation();
            return;
        }
        mBackPressed = System.currentTimeMillis();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        if (BuildConfig.DEBUG) {
            if (backStackEntryCount > 0) {//иначе приводит к ошибке java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.Object java.util.ArrayList.get(int)' on a null object reference (см. prelaunch test app 22)
                String name = fragmentManager.getBackStackEntryAt(backStackEntryCount - 1).getName();
                Timber.d("backStackEntryCount %d, top %s", backStackEntryCount, name);
            }
        }
        if (backStackEntryCount <= 1) {//last fragment,  exit

            //check this activity is root and then exit with confirmation
            if (isTaskRoot()) {
                doExitAfterConfirmation();
            }

        } else {//not last fragment, goto previous fragment
            try {
                Timber.d("not last fragment, goto previous fragment");
                fragmentManager.popBackStack();

            } catch (Exception e) {
                Timber.e(e);
            }
        }
    }

    /**
     * диалог "Хотите выйти?"
     */
    private void doExitAfterConfirmation() {
        ExitConfirmationDialog dialog = new ExitConfirmationDialog();
        dialog.show(getSupportFragmentManager(), ExitConfirmationDialog.TAG);
    }


    public void doExit() {
        Timber.d("doExit");

        cleanUp();

        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        } else {
            finish();
        }
        Runtime.getRuntime().exit(0);
    }

    public void resetTitle() {
        Timber.d("resetTitle");
        //title
        setTitle(R.string.app_name);

        //icon
        if (mMenu != null) {
            MenuItem item = mMenu.findItem(R.id.action_locale);
            if (item != null) {

                final String language = getLanguage(this);
                final boolean current_is_english = language.equals(LANGUAGE_ENGLISH);
                Drawable drawable = AppCompatResources.getDrawable(this,
                        current_is_english ? R.drawable.united_kingdom : R.drawable.russia);

                Timber.d("%s %s %b", language, LANGUAGE_ENGLISH, current_is_english);
                item.setIcon(drawable);
            }


        }

//        try {
//            int label = getPackageManager().getActivityInfo(getComponentName(), GET_META_DATA).labelRes;
//            if (label != 0) {
//                setTitle(label);
//            }
//        } catch (NameNotFoundException e) { ... }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        Timber.d("onPostResume");

        while (!deferredFragmentTransactions.isEmpty()) {
            deferredFragmentTransactions.remove().commit();
        }
    }

    @Override
    protected void onDestroy() {
        cleanUp();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_locale) {
            toggleLocale();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(setLocale(base));
        Timber.d("attachBaseContext");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Timber.i("onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

    public void toggleLocale() {
        Timber.d("toggleLocal");

        //get current lang
        final String currentLanguage = getLanguage(this);
        Timber.d("currentLanguage %s", currentLanguage);

        //toggle
        final String newLanguage = currentLanguage.equals(LANGUAGE_RUSSIAN) ? LANGUAGE_ENGLISH : LANGUAGE_RUSSIAN;
        Timber.d("newLanguage %s", newLanguage);

        //update configuration
        updateResource2(this, newLanguage);

        //save pref
        persistLanguage(this, newLanguage);

        //reset title
        resetTitle();
    }

    private void cleanUp() {

        //queue
        try {
            if (deferredFragmentTransactions.size() != 0) {
                deferredFragmentTransactions.clear();
            }

        } catch (Exception e) {
            Timber.e(e);
            //MyExceptions.log2Crashlytics(new MyLightWeightException("deferredFragmentTransactions.clear failed", e));
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        MyPermission.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    /**
     * check isRunning then call replaceFragmentInternal or add to queue
     */
    private void replaceFragment(@NonNull Fragment fragment, @Nullable String tag, boolean addStack, @Nullable String stackName) {
        Timber.d("replaceFragment, tag %s", tag != null ? tag : "null");
        if (isRunning) {
            replaceFragmentInternal(fragment, tag, addStack, stackName);

        } else {//add queue
            Timber.d("add transaction to queue");

            DeferredFragmentTransaction deferredFragmentTransaction = new DeferredFragmentTransaction() {
                @Override
                public void commit() {
                    //Timber.d("DeferredFragmentTransaction.commit");
                    replaceFragmentInternal(getReplacingFragment(), getTag(), isAddStack(), getStackName());
                }
            };

            deferredFragmentTransaction.setReplacingFragment(fragment);
            deferredFragmentTransaction.setTag(tag);
            //deferredFragmentTransaction.setAnimationType(animationType);
            deferredFragmentTransaction.setAddStack(addStack);
            deferredFragmentTransaction.setStackName(stackName);

            deferredFragmentTransactions.add(deferredFragmentTransaction);
        }
    }

//    private void removeFragment(@NonNull String tag) {
//        Timber.d("removeFragment");
//        if (isRunning) {
//            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
//            if (fragment != null) {
//                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//
//            } else {
//                Timber.w("could not find fragment to remove it %s", tag);
//            }
//
//        } else {//paused
//            Timber.w("could not remove fragment since activity paused, make deferred");//можно попробовать как deferred по аналогии с replace
//        }
//
//    }

    private void replaceFragmentInternal(@NonNull Fragment fragment, @Nullable String tag, boolean addStack, @Nullable String stackName) {
        Timber.d("replaceFragmentInternal(), isRunning %b, tag %s", isRunning, Strings.nullToEmpty(tag));
        if (fragment.isAdded()) {//to escape java.lang.IllegalStateException: Fragment already added, НЕ ВСЕГДА КАНАЕТ https://stackoverflow.com/a/13993654/6905587
            Timber.d("isAdded");
            return;
        }

        //custom animation //https://medium.com/bynder-tech/how-to-use-material-transitions-in-fragment-transactions-5a62b9d0b26b
        final FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment previousFragment = fragmentManager.findFragmentById(R.id.frame_content);
        if (previousFragment != null) {
            Fade exitFade = new Fade();
            exitFade.setDuration(500);
            previousFragment.setExitTransition(exitFade);

            Fade enterFade = new Fade();
            //enterFade.setStartDelay(500);
            enterFade.setDuration(500);
            fragment.setEnterTransition(enterFade);
        }

        //replace fragment
        final FragmentTransaction ft = fragmentManager.beginTransaction();
        if (tag != null) {//with tag
            //Timber.d("tag %s", tag);
            ft.replace(R.id.frame_content, fragment, tag);

        } else {//no tag
            ft.replace(R.id.frame_content, fragment);
        }


        if (addStack) {
            ft.addToBackStack(stackName);//might be null
        }

        ft.commit();

        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        Timber.d("addStack %b, backStackEntryCount %d", addStack, backStackEntryCount);

    }

    @UiThread
    public void showFragment(@Nullable final Bundle args, @NonNull final String tag, boolean addStack) {
        Timber.d("showFragment");
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            switch (tag) {
                case ScanFragment.TAG:
                    fragment = ScanFragment.newInstance(args);
                    break;

                case ConfigFragment.TAG:
                    if (args != null) {
                        fragment = ConfigFragment.newInstance(args);
                    }
                    break;
            }
        }

        if (!isFinishing() && fragment != null) {
            replaceFragment(fragment, tag, addStack, tag);
        }
    }


    private Fragment getAttachedFragmentByTag(@NonNull final String tag) {
        Timber.d("getAttachedFragmentByTag");
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null && fragment.isAdded() && !fragment.isDetached() && !fragment.isRemoving()) {
            return fragment;
        }
        return null;
    }

    void reScan() {
        ScanFragment fragment = (ScanFragment) getAttachedFragmentByTag(ScanFragment.TAG);
        if (fragment != null) {
            fragment.scanWiFi(this);
        }
    }

    synchronized public void setWaitScreen(boolean set) {
        Timber.d("setWaitScreen %b", set);
        runOnUiThread(new Runnable() {
            @Override
            synchronized public void run() {
                Timber.d("mWaitScreenCnt %d", mWaitScreenCnt);

                final View progressBar = findViewById(R.id.screen_wait);
                if (progressBar != null) {
                    if (set) {
                        mWaitScreenCnt++;

                    } else {
                        mWaitScreenCnt--;
                    }

                    Timber.d("mWaitScreenCnt %d", mWaitScreenCnt);

                    final int visibility = mWaitScreenCnt > 0 ? View.VISIBLE : View.GONE;
                    Timber.d("mWaitScreenCnt %d, visibility should be %d (0 - VISIBLE, 8 - GONE)", mWaitScreenCnt, visibility);

                    if (progressBar.getVisibility() != visibility) {
                        progressBar.setVisibility(visibility);
                    }

                }

            }
        });
    }




}
