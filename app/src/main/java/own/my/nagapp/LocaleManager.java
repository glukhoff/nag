package own.my.nagapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.LocaleList;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import java.util.Locale;

import timber.log.Timber;

public class LocaleManager {

    static final String LANGUAGE_ENGLISH = "en";
    private static final String LANGUAGE_DEFAULT = "en";
    static final String LANGUAGE_RUSSIAN = "ru";
    private static final String LANGUAGE_KEY = "language_key";

    public static Context setLocale(Context c) {
        return updateResources(c, getLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    static String getLanguage(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString(LANGUAGE_KEY, LANGUAGE_DEFAULT);
    }

    @SuppressLint("ApplySharedPref")
    static void persistLanguage(Context c, String language) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        // use commit() instead of apply(), because sometimes we kill the application process immediately
        // which will prevent apply() to finish
        prefs.edit().putString(LANGUAGE_KEY, language).commit();
    }

    /**
     * НЕ КАНАЕТ У МЕНЯ
     * @return context
     */
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    /**
     * бывший switchLocale()
     * @return Configuration
     */
    static Configuration updateResource2(@NonNull final Context context, @NonNull final String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        if (VERSION.SDK_INT >= VERSION_CODES.N) {//https://stackoverflow.com/a/40884242/6905587
            Timber.d("1");
            config.setLocales(new LocaleList(locale));
            config.setLayoutDirection(config.locale);

        } else if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR1) {
            Timber.d("2");
            config.setLocale(locale);
            config.setLayoutDirection(config.locale);

        } else {
            Timber.d("3");
            config.locale = locale;
        }

        final Resources resources = context.getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
        return config;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }
}