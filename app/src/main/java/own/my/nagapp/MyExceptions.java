package own.my.nagapp;


import timber.log.Timber;

public class MyExceptions implements Thread.UncaughtExceptionHandler {
    //handler
    final private Thread.UncaughtExceptionHandler mRootHandler;//current handler

    public MyExceptions() {
        mRootHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        Timber.d("uncaughtException");

        //показать причину исключения, если есть
        if (throwable.getCause() != null) {
            Timber.e(throwable.getCause());
        }

        mRootHandler.uncaughtException(thread, throwable);
    }


    public static void log2Crashlytics(Throwable throwable) {
//        Crashlytics.logException(throwable); привязать production version
        Timber.e(throwable);
    }
}
