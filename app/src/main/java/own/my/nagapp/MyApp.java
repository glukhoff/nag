package own.my.nagapp;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Looper;

import timber.log.Timber;

public class MyApp extends Application {
    private AppExecutors mAppExecutors;
    private final String TAG = "MyApp";

    @Override
    public void onCreate() {
        super.onCreate();

        //Timber
        if (BuildConfig.DEBUG) {//debug mode
            //init logger
            Timber.plant(new /*DebugTree()*/LineNoDebugTree());
        }

        mAppExecutors = new AppExecutors();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
        Timber.d("attachBaseContext");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
        Timber.d( "onConfigurationChanged: %s", newConfig.locale.getLanguage());
    }
    public AppExecutors getAppExecutors() {
        return mAppExecutors;
    }

    /**
     * check if UI thread
     */
    public static boolean isUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

}
