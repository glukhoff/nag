package own.my.nagapp;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import timber.log.Timber;

public class MyNotifier {


    static void notifyUser(@NonNull final Activity activity, @NonNull final String msg, final int duration, final int maxLines, @StringRes final int resId, final OnClickListener listener) {
        Timber.i("notifyUser %s", msg);
        final View view = activity.findViewById(R.id.coordinator) != null ?
                activity.findViewById(R.id.coordinator) :
                activity.findViewById(android.R.id.content);//ТАКЖЕ КАНАЕТ: activity.getWindow().getDecorView();

        view.post(() -> notifyUserSnackbar(view, activity, msg, duration, maxLines, resId, listener));
    }

    private static void notifyUserSnackbar(@NonNull final View view, @NonNull final Activity activity, String msg, int duration, int maxLines, @StringRes int resId, OnClickListener listener) {
        final Snackbar snackbar = Snackbar.make(view, msg, duration != 0 ? duration : Snackbar.LENGTH_LONG);

        final View snackbarView = snackbar.getView();
        final TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);

//        //set condensed font
//        final Typeface typeface = ResourcesCompat.getFont(activity, R.font.robotocondensed_light);
//        textView.setTypeface(typeface);

        // show multiple line
        if (maxLines != 0) {
            textView.setMaxLines(maxLines);
        }

        //set action
        if (resId != 0 && listener != null) {
            snackbar.setAction(resId, listener);
        }

        //show
        snackbar.show();
    }

    public static void notifyUserToast(@NonNull final Context context, @StringRes final int message) {
        final String string = context.getString(message);
        if (!TextUtils.isEmpty(string)) {
            notifyUserToast(context, string);
        }
    }

    public static void notifyUserToast(@NonNull final Context context, @NonNull final String message) {
        final MyRunnable runnable = new MyRunnable(context, message);

        if (MyApp.isUIThread()) {//this is UI
            runnable.run();

        } else {//make UI

            final Handler handler = new Handler(Looper.getMainLooper());
            handler.post(runnable);

        }
    }

    private static class MyRunnable implements Runnable {
        @NonNull private final WeakReference<Context> mContext;
        private final String mMessage;

        MyRunnable(@NonNull final Context toast, @NonNull final String message) {
            mContext = new WeakReference<>(toast);
            mMessage = message;
        }

        @Override
        public void run() {
            final Context context = mContext.get();
            if (context != null) {
                final Toast toast = Toast.makeText(context, mMessage, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        }
    }

}
