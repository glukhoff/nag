package own.my.nagapp;

import android.support.annotation.NonNull;
import timber.log.Timber.DebugTree;

class LineNoDebugTree extends DebugTree {
    @Override
    protected String createStackElementTag(@NonNull StackTraceElement element) {
        return super.createStackElementTag(element) + ":" + element.getLineNumber();
    }
}